var express = require('express');
var bp = require('body-parser');
var fs = require('fs');
var jf = require('jsonfile');
var lodash = require('lodash');
var app = express();
// setting up ejs to generate webpages
app.set('view engine', 'ejs')
app.use(express.static('public'));
// body parser setup
app.use(bp.urlencoded({ extended: true }));
app.use(bp.json());
var port = process.env.PORT || 8080;        // set our port
var router = express.Router();              // get an instance of the express Router
// trying out upload stuff
// saves a file to upload.json and then runs the functions below

var formidable = require('formidable');
let oldpath;

// Render the start page
app.get('/', function (req, res) {
    res.render('index');
});

//Render Shortcuts Pages from Links----------------------------------

app.get('/help', function (req, res) {
    res.render('help');
});

app.get('/renamer', function (req, res) {
    res.render('renamer');
});

app.get('/home', function (req, res) {
    res.render('home');
});

//-------------------------------------------------------------------

// Index form submits to /fileupload
app.post('/fileupload', function (req, res) {
    // Pulling in the entire submitted form and parsing it. Saves to a local somewhere in appdata. Bodgery gets done to oldpath.
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        oldpath = files.filetoupload.path;

        // Move onto the next section for checkboxes - done to get around multipart crap
        res.render('nextpage');
    });
});

// THIS IS FORM UPLOADING FOR RENAME
app.post('/renameupload', function (req, res) {
    // Pulling in the entire submitted form and parsing it. Saves to a local somewhere in appdata. Bodgery gets done to oldpath.
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        oldpath = files.filetoupload.path;

        // Move onto the next section for checkboxes - done to get around multipart crap
        res.render('rename2');
    });
});

//THIS IS THE PROCESSING STEP FOR RENAMING STUFF 
app.post('/nextrename', function (req, res) {
    let requested = req.body.functionality
    let tableContent = req.body.tableinput
    var data = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(renameystuff(oldpath, tableContent)));
    res.write('<a href="data:' + data + '" download="upload.json">Download Updated File Here</a>');
});


//THIS IS THE PROCESSING STEP FOR CDATA MAKER
app.post('/nextstep', function (req, res) {
    let requested = req.body.functionality
    let tableContent = req.body.tableinput
    var data = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(objectystuff(oldpath, requested, tableContent)));
    res.write('<a href="data:' + data + '" download="upload.json">Download Updated File Here</a>');
});

app.listen(port, function () {
    console.log('Example app listening on port ' + port)
});

function objectystuff(filePath, intialOptions, tableContent) {
    let file = jf.readFileSync(filePath);
    // getting value of initial component ID. Keys lists object names - only one in input. Converts it to string and all good.
    let idno = String(Object.keys(file.orchestrationJobs[0].components)[0])
    let xloca = 0
    let xcount = 1
    let name_inc = 1
    let clone = {}

    //connectors for components and stuff - ID number starting number thing
    let incrementer = 5000000
    let rando = 5000001


    // Adding new ylocation stuff
    let yloca = 0
    let ycount = 1
    let xorig = 0
    let yorig = 0
    // New variables all in here

    // stuff is grabbing the component section
    let stuff = file.orchestrationJobs[0].components
    //otherstuff is grabbing the specific component
    let otherstuff = file.orchestrationJobs[0].components[idno]


    //does this stuff fix it? -- CONNECTORS stuff but for connectors section
    let joinystuff = file.orchestrationJobs[0].successConnectors

    // Things to determine component locations
    xorig = otherstuff.x
    yorig = otherstuff.y
    yloca = yorig
    xloca = xorig
    

    let selectedoptions = []
    if (intialOptions instanceof Array) {
        selectedoptions = intialOptions
    } else {
        selectedoptions = new Array(intialOptions)
    }
    let tables = []

    selectedoptions.forEach(function (option) {
        switch (option) {
            case 'enhsql':
                enhancedSQLtrue()
                enhancedSQLfalse()
                break;

            case 'pseudo':
                psuedoColsEnabled()
                break;

            case 'basadv':
                tables = tableContent.split("\n");
                basicAdvanced(tables)
                break;

            case 'limit':
                setLimit("10")
                setLimit("10000")
                break;

            case 'tables':
                tables = tableContent.split("\n");
                otherTables(tables)
                break;

            default:
                console.log("hit teh default thing")
                break;
        };
    });

    // return with the original file variable. The referenced version has been changed, but changes to component values has been done with clones and then pushed to the referenced version.
    return (file)

    // making the clone creation a function
    function cloner() {
        clone = {}
        clone = lodash.cloneDeep(otherstuff)
    }

    // write the clone
    function addClone() {
        stuff[++idno] = clone;
    }

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
///////////CONNECTORS/////CONNECTORS/////CONNECTORS/////CONNECTORS////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

    // write the joiner or something
    function addJoin() {
        var connectorInfo = jf.readFileSync("dataConnect.json"); // This gets the connectorInfo to equal placeholder stuff
        rando = incrementer + 1 // this sets rando to equal a random number that's not random or anything
        connectorInfo.id = rando // the field is set to equal rando (which should change every time this is run)
        connectorInfo.sourceID = parseInt(idno)  // This equals ID of previous component
        connectorInfo.targetID = parseInt(idno) + 1  // This equals ID of next component - not sure if this will break shit when there isn't something that comes after but it needs to be tried

        joinystuff[++incrementer] = connectorInfo
    }

        //Lets try to get connectors working here
        function connectorShit(){
            let arrayStuff = []
            arrayStuff.push(parseInt(rando))
            let arrayAfter = []
            let varAfter = parseInt(rando) + 1
            arrayAfter.push(parseInt(varAfter))
            
            clone.inputConnectorIDs = arrayStuff
            clone.outputSuccessConnectorIDs = arrayAfter
     
        }

        function sortIDs(){
            let bleh = parseInt(idno) + 1
            //There was a bug that wasn't updating the ID - this fixes that.... I think
            clone.id = bleh
        }

//////////////////////////////////////////////////////////////////////////////

   // function shouldn't be here but it is, otherwise i'd need to declare variables elsewhere
    function xmove() {

        xloca = clone.x
        clone.x = xloca + (150 * xcount)
        ++xcount
        clone.y = yloca
        sortIDs()

        //If the xlocation of the new component is more than 1500 units to the right, instead reset the count to 1 and move it to the next row.
        if ( clone.x - xorig >= 1500 ) {
            xcount = 1
            xloca = xorig
            clone.x = xorig
            yloca = yloca + 150
            clone.y = yloca

        } 
  
    }

    // function to increment the name of new components
    function nameinc() {
        for (var allobjs in clone.parameters) {
            let obj = clone.parameters[allobjs];
            if (obj.name === "Name") {
                let slot_id = obj.slot
                let origname = clone.parameters[slot_id].elements["1"].values["1"].value
                clone.parameters[slot_id].elements["1"].values["1"].value = origname + " " + name_inc++
            }
        }
    }
    //Increment the Target Table Name
    function tableinc() {
        for (var allobjs in clone.parameters) {
            let obj = clone.parameters[allobjs];
            if (obj.name === "Target Table") {
                let slot_id = obj.slot
                let origname = clone.parameters[slot_id].elements["1"].values["1"].value
                if (!origname) {
                    clone.parameters[slot_id].elements["1"].values["1"].value = "default_" + name_inc
                } else {
                    clone.parameters[slot_id].elements["1"].values["1"].value = origname.toLowerCase() + "_" + name_inc
                }
            }
        }
    }

    //now contains the write functionality inside
    function enhancedSQLtrue() {
        var tableValues = jf.readFileSync("enhancedSQL.json");
        cloner()
        tableinc()
        nameinc()
        connectorShit()
        xmove()

        for (var allobjs in clone.parameters) {
            let obj = clone.parameters[allobjs];
            if (obj.name === "Connection Options") {
                let slot_id = obj.slot
                clone.parameters[slot_id].elements = tableValues
            }
        }
        addJoin()
        addClone()
    }

    function enhancedSQLfalse() {
        var tableValues = jf.readFileSync("enhancedSQL.json");
        cloner()
        tableinc()
        nameinc()
        connectorShit()
        xmove()

        for (var allobjs in clone.parameters) {
            let obj = clone.parameters[allobjs];
            if (obj.name === "Connection Options") {
                let slot_id = obj.slot
                tableValues["1"].values["2"].value = true
                clone.parameters[slot_id].elements = tableValues
            }
        }
        addJoin()
        addClone()
    }

    function psuedoColsEnabled() {
        tableValues = jf.readFileSync("enhancedSQL.json");
        cloner()
        tableinc()
        nameinc()
        connectorShit()
        xmove()

        for (var allobjs in clone.parameters) {
            let obj = clone.parameters[allobjs];
            if (obj.name === "Connection Options") {
                let slot_id = obj.slot
                tableValues["1"].values["1"].value = "psuedoColumns"
                tableValues["1"].values["1"].value = "*=*"
                clone.parameters[slot_id].elements = tableValues
            }
        }
        addJoin()
        addClone()
    }

    function setLimit(limit) {
        cloner()
        tableinc()
        nameinc()
        connectorShit()
        xmove()

        for (var allobjs in clone.parameters) {
            let obj = clone.parameters[allobjs];
            if (obj.name === "Limit") {
                let slot_id = obj.slot
                clone.parameters[slot_id].elements["1"].values["1"].value = limit
            }
        }
        addJoin()
        addClone()
    }

    function basicAdvanced(tableArray) {
        tableArray.forEach(function (tableName) {
            cloner()
            tableinc()
            nameinc()
            connectorShit()
            xmove()
            for (var allobjs in clone.parameters) {
                let obj = clone.parameters[allobjs];
                if (obj.name === "Basic/Advanced Mode") {
                    let slot_id = obj.slot
                    clone.parameters[slot_id].elements["1"].values["1"].value = "Advanced"
                }

                if (obj.name === "SQL Query") {
                    let slot_id = obj.slot
                    clone.parameters[slot_id].elements["1"].values["1"].value = "SELECT * FROM " + lodash.trim(tableName) + " LIMIT 100"
                }
            }
            addJoin()
            addClone()
        })
    }

    function otherTables(tableArray) {
        // This function should take a series of tables in an array and create a component for each
        // This will not work with Advanced - that would require another one (select * from {table_name} instead of setting a field)
        tableArray.forEach(function (tableName) {
            cloner()
            tableinc()
            nameinc()
            connectorShit()
            xmove()
            for (var allobjs in clone.parameters) {
                let obj = clone.parameters[allobjs];
                if (obj.name === "Data Source") {
                    let slot_id = obj.slot
                    clone.parameters[slot_id].elements["1"].values["1"].value = lodash.trim(tableName)
                }
            }
            addJoin()
            addClone()
        })
    }


}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// RENAMING STUFF IS ALL DOWN HERE////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

function renameystuff(filePath, tableContent) {
    console.log("Called Renameystuff")
    let file = jf.readFileSync(filePath);
    let counterVal=1

    let allthecomponents = file.orchestrationJobs[0].components
    for (var allobjs in allthecomponents) {
        console.log("Running the for Loop")
        let obj = allthecomponents[allobjs];
        if (obj.parameters[1].elements[1].values[1].value.includes("Copy")) {
            console.log("if")
            let prevname = obj.parameters[1].elements[1].values[1].value
            console.log(prevname)
            prevname = prevname.substr(8)
            if (prevname.includes("(")){
                prevname = prevname.slice(0,-5)
            }else{
                prevname = prevname.slice(0,-2)
            }

            console.log(prevname)
            obj.parameters[1].elements[1].values[1].value = prevname + " " + counterVal++
            console.log(obj.parameters[1].elements[1].values[1].value)

        }
    }

    return(file)

}